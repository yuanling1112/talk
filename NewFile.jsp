<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@page import="tim.CountFileHandler" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>网页计数器</title>
    
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">    
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
  </head>
  <body>
  <% 
   if(application.getAttribute("counter")==null)
   {
       application.setAttribute("counter",1);
    }
    else
    {
     String  str=application.getAttribute("counter").toString();
       int   icount=Integer.parseInt(str);
       if(session.isNew())
        {
          icount++;
        }
        application.setAttribute("counter",Integer.toString(icount));
      }
     %>
    
    您是第<%=CountFileHandler.transform(application.getAttribute("counter"))%>位. <br>
  </body>
</html>